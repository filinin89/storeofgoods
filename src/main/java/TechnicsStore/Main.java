package TechnicsStore;

import TechnicsStore.Store.Store;

import javax.xml.stream.XMLStreamReader;

public class Main {

    public static void main(String[] args) throws Exception{

        Converter converter = new Converter();

        // 1) десериализуем xml в pojo
        XMLStreamReader xmlStreamReader = converter.parseXMl();
        Store store = converter.unmarshallWithJAXB(xmlStreamReader);

        // 2) pojo сериализуем в Json
        String jsonStr = converter.GsonJsonGO(store);

        // 3) десериализуем из Json в pojo
        store = converter.gsonJSONtoPOJOgo(jsonStr);

        // 4) сериализуем из pojo в xml
        converter.marshallPOJOtoXML(store);

    }
}
