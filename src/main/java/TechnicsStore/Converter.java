package TechnicsStore;

import TechnicsStore.Store.Goods;
import TechnicsStore.Store.Store;
import TechnicsStore.Store.Subtype;
import TechnicsStore.Store.Type;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.json.JSONObject;
import org.json.XML;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamReader;
import javax.xml.transform.stream.StreamSource;
import java.io.*;
import java.text.SimpleDateFormat;

public class Converter {

    /**
     * 1  этап
     */
    public XMLStreamReader parseXMl()throws Exception{
        String input =  "src/main/resources/TechnicsStore/miniEquipment.xml"; // TechnicsStore/Equipment.xml

        // JAXP - парсим xml с помощью StAX
        XMLInputFactory xmlInputFactory = XMLInputFactory.newFactory();
        StreamSource xmlStream = new StreamSource(input); // создали поток из xml
        XMLStreamReader xmlStreamReader = xmlInputFactory.createXMLStreamReader(xmlStream);
        xmlStreamReader.nextTag(); //
        while(!xmlStreamReader.getLocalName().equals("store")) {
            xmlStreamReader.nextTag();
        }
        return xmlStreamReader;
    }


    public Store unmarshallWithJAXB(XMLStreamReader xmlStreamReader) throws Exception{
        // JAXB - десериализация


        JAXBContext jc = JAXBContext.newInstance(Store.class);
        Unmarshaller unmarshaller = jc.createUnmarshaller();
        JAXBElement<Store> jb = unmarshaller.unmarshal(xmlStreamReader, Store.class);
        xmlStreamReader.close();

        Store store = jb.getValue(); // вернет список type'ов, в каждом из которых список subtyp'ов b запишет их в соответствующие поля каждого класса в графе
        // теперь store хранит все наши объекты в графе;

        // Здесь все объекты в памяти
        System.out.println("\n\nMarshall XML to POJO:\n");
        for (Type type : store.getType()){
            System.out.println("Type: " + type.getName());
            for (Subtype subtype : type.getSubtype()){
                System.out.println("Subtype: " + subtype.getName() + "\n");
                for(Goods goods : subtype.getGoods()){ // переключаемся по готовым объектам в памяти и можем, если нужно скинуть на них ссылку в ссылочные переменные

                    System.out.println("\tManufacturer: " + goods.getManufacturer());
                    System.out.println("\tModel: " +goods.getModel());
                    System.out.println("\tReleaseDate: " +goods.getReleaseDate());
                    System.out.println("\tColor: " +goods.getColor());
                    System.out.println("\tQuantity: " +goods.getQuantity() + "\n\n");
                }
            }
        }
        return store;
    }




    /**
     * 2-ой  этап
     */


    public String GsonJsonGO(Store store)throws Exception{
        System.out.println("\n\nMarshall POJO to JSON:\n");


        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        try(Writer writerF = new FileWriter("./src/main/resources/TechnicsStore/Jsonoutput.json")){
            gson.toJson(store, writerF);
        }
        catch(IOException e){
            e.printStackTrace();
        }



        StringWriter writer = new StringWriter();
        gson.toJson(store, writer);
        String result = writer.toString();
        System.out.println(result);


        return result;
    }





    /**
     * 3-й  этап
     */
    public Store gsonJSONtoPOJOgo(String jsonString){
        StringReader reader = new StringReader(jsonString);
        Gson gson = new Gson();
        Store store = gson.fromJson(reader, Store.class);
        return store;
    }





    /**
     * 4-й  этап
     */
    public void marshallPOJOtoXML(Store store) throws Exception{ // лучше обрабатывать в try/catch

        JAXBContext jaxbContext = JAXBContext.newInstance(Store.class);
        Marshaller jaxbMarshaller = jaxbContext.createMarshaller();

        // выведем отформатированный xml
        jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);

        // Writing to console
        System.out.println("\n\nMarshall POJO to XML:\n");
        jaxbMarshaller.marshal(store, System.out);

        //Writing to XML file
        File XMLfile = new File("./src/main/resources/TechnicsStore/xmloutput.xml");
        jaxbMarshaller.marshal(store, XMLfile);


    }




    /**
     * преобразование из Json в xml
     */
    public void jsonToXMLGo(String jsonStr){

        JSONObject json = new JSONObject(jsonStr);
        String xml = XML.toString(json);

        System.out.println(xml);
    }
}
