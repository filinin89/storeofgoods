package TechnicsStore.Store;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import java.util.List;

@XmlAccessorType(XmlAccessType.FIELD)
public class Type {


    @XmlAttribute(name = "name", required = true)
    private String name;


    @XmlElement(required = true)
    private List<Subtype> subtype;

    public List<Subtype> getSubtype() {
        return subtype;
    }

    public void setSubtype(List<Subtype> subtype) {
        this.subtype = subtype;
    }

    public String getName() {
        return name;
    }


}
