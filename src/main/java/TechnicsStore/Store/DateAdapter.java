package TechnicsStore.Store;

import javax.xml.bind.annotation.adapters.XmlAdapter;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class DateAdapter extends XmlAdapter<String, LocalDate > {

    @Override
    public LocalDate unmarshal(String dateStr) throws Exception {

        // переведем дату в строке из формата yyyy-MM-dd(xsd такой принимает) в dd-MM-yyyy(для jaxb)
        String year = dateStr.substring(0, 4);
        String month = dateStr.substring(4, 8);
        String day = dateStr.substring(8, 10);
        dateStr = day + month + year;

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy");
        return LocalDate.parse(dateStr, formatter);
    }



    @Override
    public String marshal(LocalDate date) throws Exception {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy");
        return date.format(formatter);
    }
}
