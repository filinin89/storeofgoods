package TechnicsStore.Store;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

@XmlAccessorType(XmlAccessType.FIELD) @XmlRootElement
public class Store {

    @XmlElement(required = true)
    private List<Type> type; // при десереализации он сохраняет в эти списки, если его не будет, то будет: Class has no fields

    public void setType(List<Type> type) {
        this.type = type;
    }

    public List<Type> getType() {
        return type;
    }





}
